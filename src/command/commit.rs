use anyhow::Result;
use chrono::Local;
use std::env;
use std::io::stdin;

use core::database::author::Author;
use core::database::commit::Commit;
use core::database::tree::Tree;
use core::{Repository, Save};

pub fn commit() -> Result<()> {
    let mut repo = Repository::new();

    repo.index.load()?;
    let root = Tree::build(repo.index.entries()).save_all(&mut repo.database)?;

    let parent = repo.refs.read_head()?;
    let name = env::var("GIT_AUTHOR_NAME").unwrap_or_else(|_| String::from("NO_NAME"));
    let email = env::var("GIT_AUTHOR_EMAIL").unwrap_or_else(|_| String::from("NO_EMAIL"));
    let author = Author::new(&name, &email, Local::now());
    let mut message = String::new();
    stdin().read_line(&mut message)?;

    let is_root = if parent.is_none() {
        "(root-commit "
    } else {
        ""
    };

    let commit =
        Commit::new(root.oid().to_string(), parent, author, message).save(&mut repo.database)?;
    repo.refs.update_head(&commit.oid())?;

    println!(
        "[{}{}] {}",
        is_root,
        commit.oid(),
        commit.message().lines().next().unwrap_or(&"NO_MESSAGE")
    );

    Ok(())
}
