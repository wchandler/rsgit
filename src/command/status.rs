use std::collections::BTreeSet;

use core::Repository;

pub fn status() -> anyhow::Result<()> {
    let mut repo = Repository::new();
    repo.index.load()?;

    let all_files = repo.workspace.list_files(None)?;
    let untracked: BTreeSet<_> = all_files
        .iter()
        .filter(|p| !repo.index.tracking(p))
        .collect();

    for path in untracked {
        println!("?? {}", path.display());
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use anyhow::Result;
    use assert_cmd::Command;
    use std::env;
    use std::fs::{File, OpenOptions};
    use std::path::Path;
    use tempfile::{tempdir, TempDir};

    fn setup_env(path: &Path, file_names: &[&str]) -> Result<()> {
        env::set_current_dir(path)?;

        for name in file_names {
            OpenOptions::new()
                .read(true)
                .write(true)
                .create_new(true)
                .open(name)?;
        }

        let mut repo_cmd = Command::cargo_bin("rsgit").unwrap();
        //let repo_assert = repo_cmd.args(&["init", path.to_str().unwrap()]).assert();
        let repo_assert = repo_cmd.args(&["init"]).assert();
        repo_assert.success();

        Ok(())
    }

    #[test]
    fn status_sorts_untracked_by_name() -> Result<()> {
        let dir = tempdir()?;
        //setup_env(dir.path(), &["file.txt", "another.txt"])?;

        env::set_current_dir(dir.path())?;

        for name in &["file.txt", "another.txt"] {
            OpenOptions::new()
                .read(true)
                .write(true)
                .create_new(true)
                .open(name)?;
        }

        let mut repo_cmd = Command::cargo_bin("rsgit").unwrap();
        let repo_assert = repo_cmd.args(&["init"]).assert();
        repo_assert.success();

        let mut cmd = Command::cargo_bin("rsgit").unwrap();
        let assert = cmd.arg("status").assert();
        assert.success().stdout("?? another.txt\n?? file.txt\n");

        dir.close()?;
        Ok(())
    }

    #[test]
    fn status_does_not_list_files_in_index() -> Result<()> {
        let dir = tempdir()?;
        setup_env(dir.path(), &["file.txt", "another.txt"])?;

        let mut repo_cmd = Command::cargo_bin("rsgit").unwrap();
        let repo_assert = dbg!(repo_cmd.args(&["init"]).assert());
        repo_assert.success();

        let mut setup_cmd = Command::cargo_bin("rsgit").unwrap();
        let setup_assert = setup_cmd.args(&["add", "file.txt"]).assert();
        setup_assert.success();

        let mut cmd = Command::cargo_bin("rsgit").unwrap();
        let assert = cmd.arg("status").assert();
        assert.success().stdout("?? another.txt\n");

        dir.close()?;
        Ok(())
    }
}
