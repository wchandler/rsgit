use anyhow::{anyhow, Result};
use std::env;
use std::fs;
use std::fs::File;
use std::path::PathBuf;

pub fn init(target_dir: Option<&str>) -> Result<()> {
    let path = match target_dir {
        Some(d) => {
            let p = PathBuf::from(d);
            if !p.exists() {
                fs::create_dir_all(&p)?;
            }
            p.canonicalize()?
        }
        None => env::current_dir()?,
    };

    let git_path = path.join(".git");
    fs::create_dir(&git_path).map_err(|e| anyhow!("Failed to create repo dir: {}", e))?;

    for dir in ["objects", "refs"].iter() {
        fs::create_dir(&git_path.join(dir)).map_err(|e| anyhow!("Failed to create {}: {}", dir, e))?;
    }

    File::create(&git_path.join("HEAD")).map_err(|e| anyhow!("Failed to create HEAD: {}", e))?;

    println!(
        "Initialized empty rsgit repository in {}",
        git_path.display()
    );

    Ok(())
}
