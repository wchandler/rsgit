use anyhow::anyhow;
use std::path::PathBuf;

use core::database::blob::Blob;
use core::{GitError, Locked, Repository, Save};

const LOCK_ERR_MSG: &str = "\n\n
Another rsgit process seems to be running in this repository, e.g.
an editor opened by 'rsgit commit'. Please make sure all processes
are terminated then try again. If it still fails, an rsgit process
may have crashed in this repository earlier:
remove the file manually to continue.";

pub fn add(args: Vec<String>) -> anyhow::Result<()> {
    let mut repo = match Repository::new().load_index_for_update() {
        Ok(r) => r,
        Err(err) => match err {
            GitError::LockDenied(_) => {
                return Err(anyhow!("{}{}", err, LOCK_ERR_MSG));
            }
            _ => {
                return Err(err.into());
            }
        },
    };

    match run(&mut repo, args) {
        Ok(_) => {
            repo.index.write_updates()?;
            Ok(())
        }
        Err(e) => {
            repo.release_index_lock()?;
            match &e {
                GitError::WorkspaceOpenPerms(p) | GitError::WorkspaceStatPerms(p) => {
                    return Err(anyhow!(
                        "{}\nerror: unable to index file '{}'\nfatal: adding files failed",
                        e,
                        p.display()
                    ));
                }
                _ => {
                    return Err(e.into());
                }
            }
        }
    }
}

fn run(repo: &mut Repository<Locked>, args: Vec<String>) -> Result<(), GitError> {
    let mut paths = Vec::new();
    for arg in args.iter().skip(2) {
        let path = PathBuf::from(&arg);
        paths.extend(repo.workspace.list_files(Some(&path))?);
    }

    for path in paths {
        add_file(repo, path)?;
    }

    Ok(())
}

fn add_file(repo: &mut Repository<Locked>, pathname: PathBuf) -> Result<(), GitError> {
    let data = repo.workspace.read_file(&pathname)?;
    let stat = repo.workspace.stat_file(&pathname)?;

    let blob = Blob::new(data).save(&mut repo.database)?;
    repo.index.add(&pathname, blob.oid(), stat);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
}
