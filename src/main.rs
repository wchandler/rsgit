use std::env;
use std::fmt;
use std::path::PathBuf;
use std::process;
use thiserror::Error;

mod command;

fn main() {
    let args: Vec<_> = env::args().collect();

    let result = match args.get(1) {
        Some(s) if s == "add" => command::add(args),
        Some(s) if s == "commit" => command::commit(),
        Some(s) if s == "init" => command::init(args.get(2).map(|s| s.as_str())),
        Some(s) if s == "status" => command::status(),
        Some(s) => {
            exit(format!("rsgit: '{}' is not an rgit command", s), 1);
        }
        None => {
            exit("rsgit: no arguments", 1);
        }
    };

    if let Err(e) = result {
        eprintln!("fatal: {}", e);
        if let Some(src) = e.source() {
            eprintln!("{}", src);
        }
        process::exit(128);
    }
}

fn exit(msg: impl fmt::Display, code: i32) -> ! {
    eprintln!("{}", msg);
    process::exit(code);
}

#[derive(Error, Debug)]
pub enum RsgitError {
    #[error("pathspec '{0}' did not match any files")]
    FileNotFound(PathBuf),
}
