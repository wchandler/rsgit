use std::env;

fn main() {
    //let lib_dir = "libcrypto-sys/lib";
    let project_dir = env::var("CARGO_MANIFEST_DIR").unwrap();
    let lib_dir = "lib";
    let lib_name = "crypto";

    println!("cargo:rustc-link-search=native={}/{}", project_dir, lib_dir);
    println!("cargo:rustc-link-lib=static={}", lib_name);
}
