use std::os::raw::{c_int, c_uchar, c_uint, c_ulong, c_void};

#[repr(C)]
pub struct EVP_MD {
    _private: [u8; 0],
}

#[repr(C)]
pub struct EVP_MD_CTX {
    _private: [u8; 0],
}

extern "C" {
    pub fn ERR_get_error() -> c_ulong;
    pub fn ERR_error_string_n(e: c_ulong, buf: *mut c_uchar, len: usize);
    pub fn EVP_DigestInit_ex(ctx: *mut EVP_MD_CTX, crypt_type: *const EVP_MD) -> c_int;
    pub fn EVP_DigestUpdate(ctx: *mut EVP_MD_CTX, d: *const c_void, cnt: usize) -> c_int;
    pub fn EVP_DigestFinal_ex(ctx: *mut EVP_MD_CTX, md: *mut c_uchar, s: *mut c_uint) -> c_int;
    pub fn EVP_MD_CTX_new() -> *mut EVP_MD_CTX;
    pub fn EVP_MD_CTX_free(ctx: *mut EVP_MD_CTX);
    pub fn EVP_MD_size(md: *const EVP_MD) -> c_int;
    pub fn EVP_sha1() -> *const EVP_MD;
    pub fn SHA1(d: *const c_uchar, n: usize, md: *mut c_uchar);
}
