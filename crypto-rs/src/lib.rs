use std::char;
use std::convert::TryFrom;
use std::fmt;
use std::os::raw::{c_ulong, c_void};
use std::slice::Iter;

use libcrypto_sys as ffi;

pub const SHA1_LENGTH: usize = 20;
pub const SHA1_STR_LENGTH: usize = 40;

#[derive(Clone, Debug, PartialEq)]
pub struct Sha1 {
    bytes: [u8; SHA1_LENGTH],
}

impl Sha1 {
    pub fn new() -> Sha1 {
        Sha1 {
            bytes: [0; SHA1_LENGTH],
        }
    }

    pub fn one_shot(data: &[u8]) -> Sha1 {
        let mut md = [0_u8; SHA1_LENGTH];
        unsafe { ffi::SHA1(data.as_ptr(), data.len(), md.as_mut_ptr()) };

        Sha1::from(md)
    }

    pub fn iter(&self) -> Iter<u8> {
        self.bytes.iter()
    }

    pub fn bytes(&self) -> &[u8] {
        &self.bytes
    }

    pub fn ascii_bytes(&self) -> [u8; SHA1_STR_LENGTH] {
        let mut chars = [0; SHA1_STR_LENGTH];

        for (i, byte) in self.iter().enumerate() {
            let one = (byte & 0xf0) >> 4;
            let two = byte & 0x0f;
            match (
                char::from_digit(one as u32, 16),
                char::from_digit(two as u32, 16),
            ) {
                (Some(a), Some(b)) => unsafe {
                    *chars.get_unchecked_mut(i * 2) = a as u8;
                    *chars.get_unchecked_mut(i * 2 + 1) = b as u8;
                },
                _ => unreachable!(),
            }
        }
        chars
    }

    pub fn as_string(&self) -> String {
        unsafe { String::from_utf8_unchecked(self.ascii_bytes().to_vec()) }
    }
}

impl From<[u8; SHA1_LENGTH]> for Sha1 {
    fn from(bytes: [u8; SHA1_LENGTH]) -> Sha1 {
        Sha1 { bytes }
    }
}

impl TryFrom<&[u8]> for Sha1 {
    type Error = ParseSha1Error;

    fn try_from(oid: &[u8]) -> Result<Sha1, Self::Error> {
        if oid.len() != SHA1_LENGTH {
            return Err(ParseSha1Error {
                kind: Sha1ErrorKind::InvalidLength(oid.len()),
            });
        }

        let mut arr = [0; SHA1_LENGTH];
        arr.copy_from_slice(oid);
        Ok(Sha1::from(arr))
    }
}

impl TryFrom<String> for Sha1 {
    type Error = ParseSha1Error;

    fn try_from(oid: String) -> Result<Sha1, Self::Error> {
        use Sha1ErrorKind::*;

        if oid.len() != 40 {
            return Err(ParseSha1Error {
                kind: InvalidLength(oid.len()),
            });
        }

        let mut bytes = [0u8; SHA1_LENGTH];

        for (i, pair) in oid.as_bytes().chunks_exact(2).enumerate() {
            match (
                (pair[0] as char).to_digit(16),
                (pair[1] as char).to_digit(16),
            ) {
                (Some(a), Some(b)) => {
                    let val = ((a << 4) + b) as u8;
                    bytes[i] = val;
                }
                (None, _) => {
                    return Err(ParseSha1Error {
                        kind: InvalidByte(pair[0], i),
                    })
                }
                (_, None) => {
                    return Err(ParseSha1Error {
                        kind: InvalidByte(pair[1], i + 1),
                    })
                }
            }
        }
        Ok(Sha1 { bytes })
    }
}

impl fmt::Display for Sha1 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for num in self.bytes.iter() {
            write!(f, "{:02x}", num)?;
        }
        Ok(())
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct ParseSha1Error {
    kind: Sha1ErrorKind,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Sha1ErrorKind {
    InvalidLength(usize),   // Length other than 40
    InvalidByte(u8, usize), // Non-ascii_hex byte
}

impl ParseSha1Error {
    pub fn kind(&self) -> &Sha1ErrorKind {
        &self.kind
    }
}

impl std::error::Error for ParseSha1Error {}

impl fmt::Display for ParseSha1Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use Sha1ErrorKind::*;

        match self.kind {
            InvalidLength(l) => write!(
                f,
                "Could parse SHA1 from string -- invalid length {}, expected {}",
                l, SHA1_STR_LENGTH
            ),
            InvalidByte(b, loc) => write!(
                f,
                "Could not parse SHA1 from bytes -- invalid byte {:#x} at position {}",
                b, loc
            ),
        }
    }
}

#[derive(Debug)]
pub struct Digest {
    ctx: *mut ffi::EVP_MD_CTX,
}

impl Digest {
    pub fn new() -> Digest {
        let ctx = unsafe { ffi::EVP_MD_CTX_new() };
        let md = unsafe { ffi::EVP_sha1() };

        match unsafe { ffi::EVP_DigestInit_ex(ctx, md) } {
            1 => Digest { ctx },
            _ => {
                let code = unsafe { ffi::ERR_get_error() };
                panic!(LibCryptoError::new(code))
            }
        }
    }

    pub fn update(&mut self, data: &[u8]) {
        match unsafe { ffi::EVP_DigestUpdate(self.ctx, data.as_ptr() as *const c_void, data.len()) }
        {
            1 => {}
            _ => {
                let code = unsafe { ffi::ERR_get_error() };
                panic!(LibCryptoError::new(code))
            }
        }
    }

    pub fn finalize(self) -> Sha1 {
        use std::ptr;

        let mut md = [0u8; SHA1_LENGTH];
        match unsafe { ffi::EVP_DigestFinal_ex(self.ctx, md.as_mut_ptr(), ptr::null_mut()) } {
            1 => Sha1::from(md),
            _ => {
                let code = unsafe { ffi::ERR_get_error() };
                panic!(LibCryptoError::new(code))
            }
        }
    }

    pub fn one_shot(data: &[u8]) -> Sha1 {
        let mut d = Digest::new();
        d.update(data);
        d.finalize()
    }
}

impl Drop for Digest {
    fn drop(&mut self) {
        unsafe { ffi::EVP_MD_CTX_free(self.ctx) };
    }
}

impl Default for Digest {
    fn default() -> Self {
        Digest::new()
    }
}

#[derive(Clone, Copy, Debug)]
pub struct LibCryptoError {
    code: c_ulong,
}

impl LibCryptoError {
    pub fn new(code: c_ulong) -> LibCryptoError {
        LibCryptoError { code }
    }
}

impl std::error::Error for LibCryptoError {}

impl fmt::Display for LibCryptoError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut buf = [0; 256];
        unsafe { ffi::ERR_error_string_n(self.code, buf.as_mut_ptr(), buf.len()) };
        let err_str = String::from_utf8_lossy(buf.as_ref());

        write!(f, "{}", err_str)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn str_sha() {
        let input = String::from("Hello, world!");
        let output = Sha1::one_shot(input.as_bytes());
        assert_eq!(
            "943a702d06f34599aee1f8da8ef9f7296031d699",
            &output.as_string()
        );
    }

    #[test]
    fn sha_bytes() {
        let input = String::from("Hello, world!");
        let output = Sha1::one_shot(input.as_bytes());
        let expected = [
            0x94, 0x3a, 0x70, 0x2d, 0x06, 0xf3, 0x45, 0x99, 0xae, 0xe1, 0xf8, 0xda, 0x8e, 0xf9,
            0xf7, 0x29, 0x60, 0x31, 0xd6, 0x99,
        ];
        assert_eq!(expected, output.bytes());
    }
}
