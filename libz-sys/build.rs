fn main() {
    let lib_dir = "libz-sys/lib";
    let lib_name = "z";

    println!("cargo:rustc-link-search=native={}", lib_dir);
    println!("cargo:rustc-link-lib=static={}", lib_name);
}
