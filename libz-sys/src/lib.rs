use std::error;
use std::fmt;
use std::os::raw::{c_int, c_ulong};

mod ffi {
    use std::os::raw::{c_int, c_uchar, c_ulong};
    extern "C" {
        pub(super) fn compress2(
            dest: *mut c_uchar,
            destLen: *mut c_ulong,
            source: *const c_uchar,
            sourceLen: c_ulong,
            level: c_int,
        ) -> c_int;
        pub(super) fn compressBound(sourceLen: c_ulong) -> c_ulong;
        pub(super) fn uncompress(
            dest: *mut c_uchar,
            destLen: *mut c_ulong,
            source: *const c_uchar,
            sourceLen: c_ulong,
        ) -> c_int;
    }
}

#[derive(Clone, Copy, Debug)]
pub enum ZCompressLvl {
    Fast,
    Best,
}

#[derive(Clone, Debug)]
pub struct ZError(ZErrorKind);

#[derive(Clone, Debug)]
pub enum ZErrorKind {
    ErrorNo,
    StreamError,
    DataError,
    MemError,
    BufError,
    VersionError,
}

impl fmt::Display for ZError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ZErrorKind::*;
        match self.0 {
            ErrorNo => write!(f, "Error: os error"),
            StreamError => write!(f, "Error: invalid compression level set"),
            DataError => write!(f, "Error: invalid data"),
            BufError => write!(f, "Error: not enough space in output buffer"),
            MemError => write!(f, "Error: not enough memory"),
            VersionError => write!(f, "Error: incompatible libz version"),
        }
    }
}

impl From<c_int> for ZError {
    fn from(err: c_int) -> ZError {
        use ZErrorKind::*;
        match err {
            -1 => ZError(ErrorNo),
            -2 => ZError(StreamError),
            -3 => ZError(DataError),
            -4 => ZError(MemError),
            -5 => ZError(BufError),
            -6 => ZError(VersionError),
            _ => ZError(ErrorNo),
        }
    }
}

impl error::Error for ZError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}

pub fn deflate(source: &[u8], level: ZCompressLvl) -> Result<Vec<u8>, ZError> {
    let mut dest_len = unsafe { ffi::compressBound(source.len() as c_ulong) };
    let mut dest = vec![0_u8; dest_len as usize];

    let level_int: c_int = match level {
        ZCompressLvl::Fast => 1,
        ZCompressLvl::Best => 9,
    };

    let ret = unsafe {
        ffi::compress2(
            dest.as_mut_ptr(),
            &mut dest_len as *mut c_ulong,
            source.as_ptr(),
            source.len() as c_ulong,
            level_int,
        )
    };

    if ret < 0 {
        Err(ZError::from(ret))
    } else {
        dest.truncate(dest_len as usize);
        Ok(dest)
    }
}

pub fn inflate(source: &[u8]) -> Result<Vec<u8>, ZError> {
    let mut dest = vec![0_u8; 1024];
    let mut dest_len = dest.len() as c_ulong;

    let ret = unsafe {
        ffi::uncompress(
            dest.as_mut_ptr(),
            &mut dest_len as *mut c_ulong,
            source.as_ptr(),
            source.len() as c_ulong,
        )
    };

    if ret < 0 {
        Err(ZError::from(ret))
    } else {
        dest.truncate(dest_len as usize);
        Ok(dest)
    }
}
#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn compress_is_smaller() {
        let src = vec![0_u8; 255];
        let dest = deflate(&src, ZCompressLvl::Best).unwrap();
        assert!(src.len() > dest.len());
    }

    #[test]
    fn no_change_from_compression() {
        let src = String::from("Hello, world!");
        let compressed = deflate(&src.as_bytes(), ZCompressLvl::Fast).unwrap();

        let restored = inflate(&compressed).unwrap();
        let reformed = String::from_utf8_lossy(&restored);

        assert_eq!(src, reformed);
    }
}
