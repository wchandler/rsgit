use std::fs;
use std::path::{Path, PathBuf};

use crate::{Lockfile, Result, Sha1};

#[derive(Debug)]
pub struct Refs {
    pathname: PathBuf,
}

impl Refs {
    pub fn new(path: &Path) -> Refs {
        Refs {
            pathname: path.to_path_buf(),
        }
    }

    pub fn read_head(&self) -> Result<Option<String>> {
        if self.head_path().exists() {
            let content = fs::read_to_string(&self.head_path())?;
            let trimmed = content.trim_end().to_string();
            match trimmed.is_empty() {
                true => Ok(None),
                false => Ok(Some(trimmed)),
            }
        } else {
            Ok(None)
        }
    }

    pub fn update_head(&mut self, oid: &Sha1) -> Result<()> {
        let mut lock = Lockfile::new(&self.head_path()).hold_for_update()?;

        lock.write(format!("{}\n", &oid.as_string()).as_bytes())?;
        lock.commit()?;
        Ok(())
    }

    pub fn head_path(&self) -> PathBuf {
        self.pathname.join("HEAD")
    }
}
