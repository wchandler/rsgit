use bstr;
use libz_sys::{deflate, ZCompressLvl};
use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};
use std::fs::{create_dir, rename, OpenOptions};
use std::io::prelude::*;
use std::path::{Path, PathBuf};

use crate::object::{Save, Serialize};
use crate::{Result, Sha1};

pub mod author;
pub mod blob;
pub mod commit;
pub mod tree;

#[derive(Clone, Debug)]
pub struct Database {
    pathname: PathBuf,
}

impl Database {
    pub fn new(path: &Path) -> Database {
        Database {
            pathname: path.to_path_buf(),
        }
    }

    pub fn store<T: Save + Serialize>(&mut self, object: &T) -> Result<Sha1> {
        let content = object.serialize();
        let header = format!("{} {}\0", object.object_type(), content.len());
        let storable = bstr::concat(&[header.as_bytes(), &content]);

        let oid = Sha1::one_shot(&storable);
        self.write_object(&oid, &storable)?;

        Ok(oid)
    }

    fn write_object(&mut self, oid: &Sha1, content: &[u8]) -> Result<()> {
        let oid_string = oid.as_string();
        let base_path = self.pathname.join(&oid_string[0..2]);
        let object_path = base_path.join(&oid_string[2..]);
        let temp_path = base_path.join(self.generate_temp_name());

        if object_path.exists() {
            return Ok(());
        }

        if !base_path.exists() {
            create_dir(&base_path)?;
        }

        let mut file = OpenOptions::new()
            .create_new(true)
            .read(true)
            .write(true)
            .open(&temp_path)?;

        let compressed = deflate(content, ZCompressLvl::Fast)?;
        file.write_all(&compressed)?;
        drop(file);
        rename(&temp_path, &object_path)?;

        Ok(())
    }

    fn generate_temp_name(&self) -> String {
        let mut temp_name = String::from("tmp_object_");

        let mut rng = thread_rng();
        let rand_name: String = (0..6).map(|_| rng.sample(Alphanumeric)).collect();
        temp_name.push_str(&rand_name);
        temp_name
    }
}
