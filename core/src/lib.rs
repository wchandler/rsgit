pub mod database;
pub mod error;
pub mod index;
pub mod lockfile;
pub mod object;
pub mod refs;
pub mod repository;
mod utils;
pub mod workspace;

type Result<T> = std::result::Result<T, GitError>;

pub use crate::object::{Save, Serialize};
pub use crypto_rs::{Digest, Sha1};
pub use database::Database;
pub use error::GitError;
pub use index::Index;
pub use lockfile::{Locked, Lockfile, Unlocked};
pub use refs::Refs;
pub use repository::Repository;
pub use workspace::Workspace;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
