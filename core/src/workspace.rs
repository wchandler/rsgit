use std::fs;
use std::fs::Metadata;
use std::io::ErrorKind;
use std::os::unix::ffi::OsStrExt;
use std::path::{Path, PathBuf};
use walkdir::WalkDir;

use crate::{GitError, Result};

const IGNORE: &[u8] = b".git";

#[derive(Clone, Debug)]
pub struct Workspace {
    pathname: PathBuf,
}

impl Workspace {
    pub fn new(path: &Path) -> Workspace {
        Workspace {
            pathname: path.to_path_buf(),
        }
    }

    pub fn list_files(&self, path: Option<&Path>) -> Result<Vec<PathBuf>> {
        let path = match path {
            Some(p) => p,
            None => &self.pathname,
        };

        // TODO this derefs symlinks
        let path = match path.canonicalize() {
            Ok(p) => p,
            Err(e) => match e.kind() {
                ErrorKind::NotFound => {
                    eprintln!("Failed to canonicalize {}", path.display());
                    return Err(GitError::WorkspaceMissingFile(path.to_owned()));
                }
                _ => {
                    return Err(e.into());
                }
            },
        };

        if path.is_file() {
            return Ok(vec![path.strip_prefix(&self.pathname)?.to_owned()]);
        }

        let paths: Vec<_> = WalkDir::new(path)
            .sort_by(|a, b| a.path().as_os_str().cmp(b.path().as_os_str()))
            .into_iter()
            .filter_entry(|e| e.file_name().as_bytes() != IGNORE)
            .filter_map(|e| e.ok())
            .filter(|e| e.file_type().is_file())
            .map(|e| e.into_path())
            .collect();

        let stripped_paths = paths
            .into_iter()
            .filter_map(|p| {
                p.strip_prefix(&self.pathname)
                    .ok()
                    .filter(|p| p.as_os_str() != "")
                    .map(|p| p.to_path_buf())
            })
            .collect();

        Ok(stripped_paths)
    }

    pub fn read_file(&self, path: &Path) -> Result<Vec<u8>> {
        match fs::read(path) {
            Ok(data) => Ok(data),
            Err(e) => match e.kind() {
                ErrorKind::PermissionDenied => {
                    return Err(GitError::WorkspaceOpenPerms(path.to_owned()));
                }
                _ => return Err(e.into()),
            },
        }
    }

    pub fn stat_file(&self, path: &Path) -> Result<Metadata> {
        match fs::metadata(path) {
            Ok(meta) => Ok(meta),
            Err(e) => match e.kind() {
                ErrorKind::PermissionDenied => {
                    return Err(GitError::WorkspaceStatPerms(path.to_owned()));
                }
                _ => return Err(e.into()),
            },
        }
    }
}
