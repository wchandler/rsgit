use bstr;
use std::convert::{TryFrom, TryInto};
use std::ffi::OsString;
use std::fs::Metadata;
use std::os::unix::ffi::{OsStrExt, OsStringExt};
use std::os::unix::fs::{MetadataExt, PermissionsExt};
use std::path::{Path, PathBuf};

use super::ENTRY_BLOCK;
use crate::object::{Object, ObjectType, Serialize};
use crate::{GitError, Result, Sha1};

const EXECUTABLE_MODE: u32 = 0o100755;
const REGULAR_MODE: u32 = 0o100644;
const MAX_PATH_SIZE: u16 = 0xfff;

pub const REGULAR_MODE_BYTES: &[u8] = b"100644";
pub const EXECUTABLE_MODE_BYTES: &[u8] = b"100755";

#[derive(Clone, Copy, Debug)]
pub enum Mode {
    Regular,
    Executable,
}

impl Mode {
    pub fn as_int(&self) -> u32 {
        match self {
            Mode::Regular => REGULAR_MODE,
            Mode::Executable => EXECUTABLE_MODE,
        }
    }
}

impl TryFrom<&[u8]> for Mode {
    type Error = GitError;

    fn try_from(data: &[u8]) -> Result<Mode> {
        let int = u32::from_be_bytes(data.try_into()?);

        match int {
            REGULAR_MODE => Ok(Mode::Regular),
            EXECUTABLE_MODE => Ok(Mode::Executable),
            _ => Err(GitError::IndexInvalidFileMode(int).into()),
        }
    }
}

#[derive(Clone, Debug)]
pub struct Entry {
    ctime: u32,
    ctime_nsec: u32,
    mtime: u32,
    mtime_nsec: u32,
    dev: u32,
    ino: u32,
    mode: Mode,
    uid: u32,
    gid: u32,
    size: u32,
    oid: Sha1,
    flags: u16,
    pub path: PathBuf,
}

impl Entry {
    pub fn create(path: &Path, oid: &Sha1, stat: Metadata) -> Entry {
        let mode = if 0o111 & stat.permissions().mode() == 0 {
            Mode::Regular
        } else {
            Mode::Executable
        };

        let pathlen = path.as_os_str().len() as u16;
        let flags = pathlen.min(MAX_PATH_SIZE);

        Entry {
            ctime: stat.ctime() as u32,
            ctime_nsec: stat.ctime_nsec() as u32,
            mtime: stat.mtime() as u32,
            mtime_nsec: stat.mtime_nsec() as u32,
            dev: stat.dev() as u32,
            ino: stat.ino() as u32,
            mode,
            uid: stat.uid(),
            gid: stat.gid(),
            size: stat.size() as u32,
            oid: oid.clone(),
            flags,
            path: path.into(),
        }
    }

    pub fn mode(&self) -> &[u8] {
        match self.mode {
            Mode::Regular => REGULAR_MODE_BYTES,
            Mode::Executable => EXECUTABLE_MODE_BYTES,
        }
    }

    pub fn oid(&self) -> &Sha1 {
        &self.oid
    }
}

impl Object for Entry {
    fn object_type(&self) -> ObjectType {
        ObjectType::Entry
    }
}

impl Serialize for Entry {
    fn serialize(&self) -> Vec<u8> {
        let mut data = bstr::concat(&[
            &self.ctime.to_be_bytes(),
            &self.ctime_nsec.to_be_bytes(),
            &self.mtime.to_be_bytes(),
            &self.mtime_nsec.to_be_bytes(),
            &self.dev.to_be_bytes(),
            &self.ino.to_be_bytes(),
            &self.mode.as_int().to_be_bytes(),
            &self.uid.to_be_bytes(),
            &self.gid.to_be_bytes(),
            &self.size.to_be_bytes(),
            self.oid.bytes(),
            &self.flags.to_be_bytes(),
            self.path.as_os_str().as_bytes(),
            b"\0",
        ]);

        while data.len() % ENTRY_BLOCK != 0 {
            data.push(b'\0');
        }

        data
    }
}

impl TryFrom<&[u8]> for Entry {
    type Error = GitError;

    fn try_from(data: &[u8]) -> Result<Entry> {
        if data.len() < super::ENTRY_MIN_SIZE {
            return Err(GitError::IndexInvalidLength {
                received: data.len(),
                min: super::ENTRY_MIN_SIZE,
            }
            .into());
        }

        let path_vec: Vec<_> = data[62..]
            .iter()
            .cloned()
            .take_while(|&i| i != b'\0')
            .collect();
        let path: PathBuf = OsString::from_vec(path_vec).into();

        let entry = Entry {
            ctime: u32::from_be_bytes(data[..4].try_into()?),
            ctime_nsec: u32::from_be_bytes(data[4..8].try_into()?),
            mtime: u32::from_be_bytes(data[8..12].try_into()?),
            mtime_nsec: u32::from_be_bytes(data[12..16].try_into()?),
            dev: u32::from_be_bytes(data[16..20].try_into()?),
            ino: u32::from_be_bytes(data[20..24].try_into()?),
            mode: Mode::try_from(&data[24..28])?,
            uid: u32::from_be_bytes(data[28..32].try_into()?),
            gid: u32::from_be_bytes(data[32..36].try_into()?),
            size: u32::from_be_bytes(data[36..40].try_into()?),
            oid: Sha1::try_from(&data[40..60])?,
            flags: u16::from_be_bytes(data[60..62].try_into()?),
            path,
        };

        Ok(entry)
    }
}
