use super::author::Author;
use crate::database::Database;
use crate::object::{Object, ObjectType, Save, Saved, Serialize, Unsaved};
use crate::{Result, Sha1};

pub struct Commit<S> {
    tree: String,
    parent: Option<String>,
    author: Author,
    message: String,
    obj_t: ObjectType,
    state: S,
}

impl<S> Commit<S> {
    fn serialize_parent(&self) -> String {
        match &self.parent {
            Some(p) => format!("parent {}\n", p),
            None => String::new(),
        }
    }
}

impl<S> Object for Commit<S> {
    fn object_type(&self) -> ObjectType {
        self.obj_t
    }
}

impl<S> Serialize for Commit<S> {
    fn serialize(&self) -> Vec<u8> {
        let content = format!(
            "tree {}\n{}author {}\ncommitter {}\n\n{}",
            self.tree,
            &self.serialize_parent(),
            self.author,
            self.author,
            self.message
        );

        content.into_bytes()
    }
}

impl Commit<Unsaved> {
    pub fn new(
        tree: String,
        parent: Option<String>,
        author: Author,
        message: String,
    ) -> Commit<Unsaved> {
        Commit {
            parent,
            tree,
            author,
            message,
            obj_t: ObjectType::Commit,
            state: Unsaved {},
        }
    }
}

impl Save for Commit<Unsaved> {
    type Saved = Commit<Saved>;

    #[must_use]
    fn save(self, db: &mut Database) -> Result<Commit<Saved>> {
        let oid = db.store(&self)?;
        Ok(Commit {
            tree: self.tree,
            parent: self.parent,
            author: self.author,
            message: self.message,
            obj_t: self.obj_t,
            state: Saved { oid },
        })
    }
}

impl Commit<Saved> {
    pub fn oid(&self) -> &Sha1 {
        &self.state.oid
    }
    pub fn message(&self) -> &str {
        &self.message
    }
}
