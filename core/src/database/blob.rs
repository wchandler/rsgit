use crate::database::Database;
use crate::object::*;
use crate::Result;
use crypto_rs::Sha1;

#[derive(Clone, Debug)]
pub struct Blob<S> {
    data: Vec<u8>,
    obj_t: ObjectType,
    state: S,
}

impl<S> Object for Blob<S> {
    fn object_type(&self) -> ObjectType {
        self.obj_t
    }
}

impl<S> Serialize for Blob<S> {
    fn serialize(&self) -> Vec<u8> {
        self.data.clone()
    }
}

impl Blob<Unsaved> {
    pub fn new(data: Vec<u8>) -> Blob<Unsaved> {
        Blob {
            data,
            state: Unsaved {},
            obj_t: ObjectType::Blob,
        }
    }
}

impl Save for Blob<Unsaved> {
    type Saved = Blob<Saved>;

    #[must_use]
    fn save(self, db: &mut Database) -> Result<Blob<Saved>> {
        let oid = db.store(&self)?;
        Ok(Blob {
            data: self.data,
            state: Saved { oid },
            obj_t: self.obj_t,
        })
    }
}

impl Blob<Saved> {
    pub fn oid(&self) -> &Sha1 {
        &self.state.oid
    }
}
