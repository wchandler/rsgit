use bstr;
use indexmap::IndexMap;
use std::collections::VecDeque;
use std::ffi::{OsStr, OsString};
use std::os::unix::ffi::OsStrExt;

use crate::index::entry::Entry;
use crate::object::{Object, ObjectType, Save, Saved, Serialize, Unsaved};
use crate::{Database, Result, Sha1};

pub const DIRECTORY_MODE_BYTES: &[u8] = b"40000";

#[derive(Clone, Debug)]
pub enum UnsavedObj {
    Entry(Entry),
    Tree(Tree<Unsaved, UnsavedObj>),
}

#[derive(Clone, Debug)]
pub enum SavedObj {
    Entry(Entry),
    Tree(Tree<Saved, SavedObj>),
}

impl SavedObj {
    pub fn mode(&self) -> &[u8] {
        match self {
            SavedObj::Entry(e) => e.mode(),
            SavedObj::Tree(_) => DIRECTORY_MODE_BYTES,
        }
    }
}

impl UnsavedObj {
    fn save(self, db: &mut Database) -> Result<SavedObj> {
        let saved_obj = match self {
            UnsavedObj::Entry(e) => SavedObj::Entry(e),
            UnsavedObj::Tree(t) => SavedObj::Tree(t.save_all(db)?),
        };
        Ok(saved_obj)
    }
}

#[derive(Clone, Debug)]
pub struct Tree<S, T> {
    pub entries: IndexMap<OsString, T>,
    obj_t: ObjectType,
    state: S,
}

impl Tree<Unsaved, UnsavedObj> {
    pub fn new() -> Tree<Unsaved, UnsavedObj> {
        Tree {
            entries: IndexMap::new(),
            obj_t: ObjectType::Tree,
            state: Unsaved {},
        }
    }
}

impl Tree<Unsaved, SavedObj> {
    fn new_saved() -> Tree<Unsaved, SavedObj> {
        Tree {
            entries: IndexMap::new(),
            obj_t: ObjectType::Tree,
            state: Unsaved {},
        }
    }
}

impl<S, T> Object for Tree<S, T> {
    fn object_type(&self) -> ObjectType {
        self.obj_t
    }
}

impl Tree<Unsaved, UnsavedObj> {
    pub fn build<'a, I>(entries: I) -> Tree<Unsaved, UnsavedObj>
    where
        I: IntoIterator<Item = &'a Entry>,
    {
        let mut root = Tree::new();

        for entry in entries.into_iter() {
            let name = entry.path.clone();
            let mut path: VecDeque<_> = name.iter().collect();

            if let Some(name) = path.pop_back() {
                root.add_entry(path, &name, entry.clone())
            }
        }

        root
    }

    pub fn add_entry(&mut self, mut path: VecDeque<&OsStr>, name: &OsStr, entry: Entry) {
        match path.iter().next() {
            Some(root) => {
                let value = self
                    .entries
                    .entry(root.into())
                    .or_insert(UnsavedObj::Tree(Tree::new()));

                if let UnsavedObj::Tree(tree) = value {
                    path.pop_front();
                    tree.add_entry(path, name, entry);
                }
            }
            None => {
                self.entries
                    .insert(name.to_os_string(), UnsavedObj::Entry(entry));
            }
        }
    }

    pub fn save_all(self, db: &mut Database) -> Result<Tree<Saved, SavedObj>> {
        let mut new_tree = Tree::new_saved();

        for (path, value) in self.entries.into_iter() {
            let saved_value = value.save(db)?;
            new_tree.entries.insert(path, saved_value);
        }

        new_tree.save(db)
    }
}

impl Tree<Saved, SavedObj> {
    pub fn oid(&self) -> &Sha1 {
        &self.state.oid
    }
}

impl Save for Tree<Unsaved, SavedObj> {
    type Saved = Tree<Saved, SavedObj>;

    #[must_use]
    fn save(self, db: &mut Database) -> Result<Tree<Saved, SavedObj>> {
        let oid = db.store(&self)?;
        Ok(Tree {
            entries: self.entries,
            obj_t: self.obj_t,
            state: Saved { oid },
        })
    }
}

impl<S> Serialize for Tree<S, SavedObj> {
    fn serialize(&self) -> Vec<u8> {
        self.entries
            .iter()
            .map(|(name, entry)| match entry {
                SavedObj::Tree(t) => bstr::concat(&[
                    entry.mode(),
                    b" ",
                    name.as_os_str().as_bytes(),
                    b"\0",
                    &t.oid().bytes(),
                ]),
                SavedObj::Entry(e) => bstr::concat(&[
                    entry.mode(),
                    b" ",
                    name.as_os_str().as_bytes(),
                    b"\0",
                    &e.oid().bytes(),
                ]),
            })
            .flatten()
            .collect()
    }
}
