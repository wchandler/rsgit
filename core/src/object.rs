use std::fmt;

use crate::{Database, Result, Sha1};

pub trait Object {
    fn object_type(&self) -> ObjectType;
}

pub trait Save: Object {
    type Saved;

    fn save(self, db: &mut Database) -> Result<Self::Saved>;
}

pub trait Serialize: Object {
    fn serialize(&self) -> Vec<u8>;
}

#[derive(Clone, Debug)]
pub struct Unsaved;

#[derive(Clone, Debug)]
pub struct Saved {
    pub oid: Sha1,
}

#[derive(Clone, Copy, Debug)]
pub enum ObjectType {
    Blob,
    Commit,
    Entry,
    Tree,
}

impl fmt::Display for ObjectType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ObjectType::*;
        match &self {
            Blob => write!(f, "blob"),
            Commit => write!(f, "commit"),
            Entry => write!(f, "entry"),
            Tree => write!(f, "tree"),
        }
    }
}
