use std::env;
use std::path::{Path, PathBuf};

use crate::{Database, Index, Locked, Refs, Result, Unlocked, Workspace};

#[derive(Debug)]
pub struct Repository<L> {
    pub git_path: PathBuf,
    pub database: Database,
    pub index: Index<L>,
    pub refs: Refs,
    pub workspace: Workspace,
}

impl Repository<Unlocked> {
    pub fn new() -> Repository<Unlocked> {
        let path = env::current_dir()
            .expect("unable to get current directory")
            .join(".git");

        Repository {
            git_path: path.to_path_buf(),
            database: Database::new(&path.join("objects")),
            index: Index::new(&path.join("index")),
            refs: Refs::new(&path),
            workspace: Workspace::new(path.parent().unwrap()),
        }
    }

    pub fn lock_index(self) -> Result<Repository<Locked>> {
        let index = self.index.lock()?;

        Ok(Repository {
            git_path: self.git_path,
            database: self.database,
            index,
            refs: self.refs,
            workspace: self.workspace,
        })
    }

    pub fn load_index_for_update(self) -> Result<Repository<Locked>> {
        let index = self.index.load_for_update()?;

        Ok(Repository {
            git_path: self.git_path,
            database: self.database,
            index,
            refs: self.refs,
            workspace: self.workspace,
        })
    }
}

impl Repository<Locked> {
    pub fn write_index_updates(self) -> Result<Repository<Unlocked>> {
        let index = self.index.write_updates()?;

        Ok(Repository {
            git_path: self.git_path,
            database: self.database,
            index,
            refs: self.refs,
            workspace: self.workspace,
        })
    }

    pub fn release_index_lock(self) -> Result<Repository<Unlocked>> {
        let index = self.index.release_lock()?;

        Ok(Repository {
            git_path: self.git_path,
            database: self.database,
            index,
            refs: self.refs,
            workspace: self.workspace,
        })
    }
}

impl From<&Path> for Repository<Unlocked> {
    fn from(path: &Path) -> Repository<Unlocked> {
        Repository {
            git_path: path.to_path_buf(),
            database: Database::new(&path.join("objects")),
            index: Index::new(&path.join("index")),
            refs: Refs::new(path),
            workspace: Workspace::new(path.parent().unwrap()),
        }
    }
}
