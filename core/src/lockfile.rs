use std::fs;
use std::fs::{File, OpenOptions};
use std::io::{ErrorKind, Write};
use std::path::{Path, PathBuf};

use crate::{GitError, Result};

#[derive(Debug)]
pub struct Lockfile<S> {
    pub file_path: PathBuf,
    pub lock_path: PathBuf,
    state: S,
}

impl Lockfile<Unlocked> {
    pub fn new(path: &Path) -> Lockfile<Unlocked> {
        Lockfile {
            file_path: path.to_path_buf(),
            lock_path: path.with_extension("lock"),
            state: Unlocked,
        }
    }

    pub fn hold_for_update(self) -> Result<Lockfile<Locked>> {
        let lockfile_result = OpenOptions::new()
            .create_new(true)
            .read(true)
            .write(true)
            .open(&self.lock_path);

        let lockfile = match lockfile_result {
            Ok(l) => l,
            Err(err) => match err.kind() {
                ErrorKind::AlreadyExists => return Err(GitError::LockDenied(self.lock_path)),
                _ => return Err(err.into()),
            },
        };

        Ok(Lockfile {
            file_path: self.file_path,
            lock_path: self.lock_path.clone(),
            state: Locked { lock_fd: lockfile, lock_path: self.lock_path },
        })
    }
}

impl Lockfile<Locked> {
    pub fn write(&mut self, bytes: &[u8]) -> Result<()> {
        self.state.lock_fd.write_all(bytes)?;
        self.state.lock_fd.sync_all().map_err(|e| e.into())
    }

    pub fn commit(self) -> Result<Lockfile<Unlocked>> {
        fs::rename(&self.lock_path, &self.file_path)?;

        Ok(Lockfile {
            file_path: self.file_path,
            lock_path: self.lock_path,
            state: Unlocked,
        })
    }

    pub fn rollback(self) -> Result<Lockfile<Unlocked>> {
        Ok(Lockfile {
            file_path: self.file_path,
            lock_path: self.lock_path,
            state: Unlocked,
        })
    }
}

impl Drop for Locked {
    fn drop(&mut self) {
        // Don't panic if lockfile doesn't exist
        let _ = fs::remove_file(&self.lock_path);
    }
}

#[derive(Debug)]
pub struct Locked {
    lock_fd: File,
    lock_path: PathBuf,
}

#[derive(Debug)]
pub struct Unlocked;
