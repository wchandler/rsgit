use std::io;
use std::path::PathBuf;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum GitError {
    #[error("Checksum does not match value stored on disk\n  expected: {expected}\n  received: {received}")]
    IndexInvalidHash { expected: String, received: String },
    #[error("Unrecognized mode: {0:#06o}")]
    IndexInvalidFileMode(u32),
    #[error("Invalid index entry -- received {received} bytes, minimum length is {min} bytes")]
    IndexInvalidLength { received: usize, min: usize },
    #[error("Invalid signature -- expected: {expected:#?}, received: {received:#?}")]
    IndexInvalidSignature {
        expected: &'static [u8],
        received: Vec<u8>,
    },
    #[error("Invalid version -- expected: {expected}, received {received}")]
    IndexInvalidVersion { expected: u32, received: u32 },
    #[error(transparent)]
    IOError(#[from] io::Error),
    #[error(transparent)]
    LibZError(#[from] libz_sys::ZError),
    #[error("Unable to create {0}: File exists.")]
    LockDenied(PathBuf),
    #[error(transparent)]
    Sha1Error(#[from] crypto_rs::ParseSha1Error),
    #[error(transparent)]
    StripPrefixError(#[from] std::path::StripPrefixError),
    #[error(transparent)]
    TryFromSliceError(#[from] std::array::TryFromSliceError),
    #[error("pathspec '{0}' did not match any files")]
    WorkspaceMissingFile(PathBuf),
    #[error("open(\"{0}\"): Permission denied")]
    WorkspaceOpenPerms(PathBuf),
    #[error("stat(\"{0}\"): Permission denied")]
    WorkspaceStatPerms(PathBuf),
}
