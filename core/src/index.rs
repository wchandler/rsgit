use bstr;
use std::collections::{btree_map::Values, BTreeMap, BTreeSet};
use std::convert::{TryFrom, TryInto};
use std::fs::{File, Metadata};
use std::io;
use std::io::{prelude::*, ErrorKind};
use std::mem;
use std::path::{Path, PathBuf};

use crate::index::entry::Entry;
use crate::utils;
use crate::{Digest, GitError, Locked, Lockfile, Result, Serialize, Sha1, Unlocked};

pub mod entry;

const CHECKSUM_SIZE: usize = 20;
const ENTRY_BLOCK: usize = 8;
const ENTRY_MIN_SIZE: usize = 64;
const HEADER_SIZE: usize = 12;
const SIGNATURE: &'static [u8; 4] = b"DIRC";
const VERSION: u32 = 2;

#[derive(Debug)]
pub struct Index<L> {
    entries: BTreeMap<PathBuf, Entry>,
    parents: BTreeMap<PathBuf, BTreeSet<PathBuf>>,
    digest: Digest,
    changed: bool,
    lockfile: Lockfile<L>,
}

impl<L> Index<L> {
    pub fn add(&mut self, path: &Path, oid: &Sha1, stat: Metadata) {
        let entry = Entry::create(path, oid, stat);

        self.discard_conflicts(&entry);
        self.store_entry(entry);
    }

    pub fn store_entry(&mut self, entry: Entry) {
        if let Some(parents) = utils::parent_directories(&entry.path) {
            for dir in parents {
                let children = self
                    .parents
                    .entry(PathBuf::from(dir))
                    .or_insert_with(BTreeSet::new);

                children.insert(entry.path.clone());
            }
        }
        self.entries.insert(entry.path.clone(), entry);

        self.changed = true;
    }

    pub fn index_path(&self) -> &Path {
        &self.lockfile.file_path
    }

    pub fn entries(&self) -> Values<PathBuf, Entry> {
        self.entries.values()
    }

    pub fn tracking(&self, path: &Path) -> bool {
        self.entries.contains_key(path)
    }

    pub fn load(&mut self) -> Result<()> {
        self.entries.clear();
        self.changed = false;

        let file = match File::open(self.index_path()) {
            Ok(f) => Some(f),
            Err(e) => match e.kind() {
                ErrorKind::NotFound => None,
                _ => return Err(e.into()),
            },
        };

        if let Some(mut f) = file {
            self.read_entries(&mut f)?;
            self.verify_checksum(&mut f)?;
        }

        Ok(())
    }

    pub fn read_entries(&mut self, file: &mut File) -> Result<()> {
        let ct = self.read_header(file)?;

        for _ in 0..ct {
            let mut entry_buf = self.read_entry_start(file)?;

            while entry_buf[entry_buf.len() - 1] != b'\0' {
                let block = self.read_entry_block(file)?;
                entry_buf.extend(&block);
            }

            let entry = Entry::try_from(&entry_buf[..])?;
            self.store_entry(entry);
        }

        Ok(())
    }

    fn discard_conflicts(&mut self, entry: &Entry) {
        if let Some(parent_dirs) = utils::parent_directories(&entry.path) {
            for parent in parent_dirs {
                self.remove_entry(parent);
            }
        }

        if let Some(children) = self.parents.remove(&entry.path) {
            for child in children {
                self.remove_entry(&child);
            }
        }
    }

    fn remove_entry(&mut self, path: &Path) {
        if self.entries.remove(path).is_none() {
            return;
        }

        if let Some(parents) = utils::parent_directories(path) {
            for dir in parents {
                self.parents.get_mut(dir).map(|d| d.remove(path));
            }
        }
    }

    fn read(&mut self, buf: &mut [u8], file: &mut File) -> Result<()> {
        let ct = file.read(buf)?;
        if ct != buf.len() {
            return Err(io::Error::new(
                ErrorKind::UnexpectedEof,
                "Unexpected end-of-file while reading index",
            )
            .into());
        }

        self.digest.update(&buf);
        Ok(())
    }

    fn read_header(&mut self, file: &mut File) -> Result<u32> {
        let mut buf = [0; HEADER_SIZE];
        self.read(&mut buf, file)?;

        let signature = &buf[..4];
        if signature != SIGNATURE {
            return Err(GitError::IndexInvalidSignature {
                expected: SIGNATURE,
                received: Vec::from(signature),
            }
            .into());
        }

        let version = u32::from_be_bytes(buf[4..8].try_into()?);
        if version != VERSION {
            return Err(GitError::IndexInvalidVersion {
                expected: VERSION,
                received: version,
            }
            .into());
        }

        let count = u32::from_be_bytes(buf[8..].try_into()?);

        Ok(count)
    }

    fn read_entry_start(&mut self, file: &mut File) -> Result<Vec<u8>> {
        let mut buf = vec![0; ENTRY_MIN_SIZE];
        self.read(&mut buf, file)?;

        Ok(buf)
    }

    fn read_entry_block(&mut self, file: &mut File) -> Result<[u8; ENTRY_BLOCK]> {
        let mut buf = [0; ENTRY_BLOCK];
        let ct = file.read(&mut buf)?;

        if ct != ENTRY_BLOCK {
            return Err(io::Error::new(
                ErrorKind::UnexpectedEof,
                "Unexpected end-of-file while reading index",
            )
            .into());
        }

        self.digest.update(&buf);
        Ok(buf)
    }

    fn verify_checksum(&mut self, file: &mut File) -> Result<()> {
        let mut buf = [0; CHECKSUM_SIZE];
        file.read(&mut buf[..])?;

        let digest = mem::take(&mut self.digest);

        let hash = digest.finalize();
        if hash.bytes() != buf {
            return Err(GitError::IndexInvalidHash {
                expected: Sha1::from(buf).as_string(),
                received: hash.as_string(),
            }
            .into());
        }

        Ok(())
    }
}

impl Index<Unlocked> {
    pub fn new(path: &Path) -> Index<Unlocked> {
        Index {
            entries: BTreeMap::new(),
            parents: BTreeMap::new(),
            digest: Digest::new(),
            changed: false,
            lockfile: Lockfile::new(path),
        }
    }

    #[must_use]
    pub fn load_for_update(self) -> Result<Index<Locked>> {
        let mut locked_index = self.lock()?;
        if let Err(e) = locked_index.load() {
            locked_index.release_lock()?;
            return Err(e);
        }

        Ok(locked_index)
    }

    #[must_use]
    pub fn lock(self) -> Result<Index<Locked>> {
        let locked = self.lockfile.hold_for_update()?;

        Ok(Index {
            entries: self.entries,
            parents: self.parents,
            digest: self.digest,
            changed: self.changed,
            lockfile: locked,
        })
    }
}

impl Index<Locked> {
    pub fn write_updates(mut self) -> Result<Index<Unlocked>> {
        if !self.changed {
            let unlocked = self.lockfile.rollback()?;

            return Ok(Index {
                entries: self.entries,
                parents: self.parents,
                digest: Digest::new(),
                changed: false,
                lockfile: unlocked,
            });
        }

        let len = self.entries.len() as u32;
        let header = bstr::concat(&[b"DIRC", &2u32.to_be_bytes(), &len.to_be_bytes()]);
        self.write(&header)?;

        let data: Vec<_> = self
            .entries
            .iter()
            .map(|(_, entry)| entry.serialize())
            .flatten()
            .collect();
        self.write(&data)?;

        self.finish_write()
    }

    fn write(&mut self, data: &[u8]) -> Result<()> {
        self.lockfile.write(data)?;
        self.digest.update(data);

        Ok(())
    }

    pub fn finish_write(mut self) -> Result<Index<Unlocked>> {
        let hash = self.digest.finalize();
        self.lockfile.write(hash.bytes())?;

        let unlocked = self.lockfile.commit()?;

        Ok(Index {
            entries: self.entries,
            parents: self.parents,
            digest: Digest::new(),
            changed: false,
            lockfile: unlocked,
        })
    }

    pub fn release_lock(self) -> Result<Index<Unlocked>> {
        let lockfile = self.lockfile.rollback()?;

        Ok(Index {
            entries: self.entries,
            parents: self.parents,
            digest: Digest::new(),
            changed: false,
            lockfile,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::convert::TryFrom;
    use std::fs;
    use tempfile::tempdir;

    fn build_index() -> Result<(Index<Unlocked>, Sha1, Metadata)> {
        let tmp_dir = tempdir()?;

        let tmp_path = tmp_dir.path().join("test_file");
        let _file = File::create(&tmp_path)?;
        let stat = fs::metadata(tmp_path)?;

        let index_path = tmp_dir.path().join("index");
        let index = Index::new(&index_path);

        let rand: Vec<u8> = (0..20).map(|_| rand::random::<u8>()).collect();
        let oid = Sha1::try_from(&rand[..])?;

        Ok((index, oid, stat))
    }

    #[test]
    fn add_one_file() -> Result<()> {
        let (mut index, oid, stat) = build_index()?;

        index.add(Path::new("alice.txt"), &oid, stat);
        assert_eq!(
            vec![Path::new("alice.txt")],
            index.entries.keys().collect::<Vec<_>>(),
        );

        Ok(())
    }

    #[test]
    fn replace_file_with_dir() -> Result<()> {
        let (mut index, oid, stat) = build_index()?;
        index.add(Path::new("alice.txt"), &oid, stat.clone());
        index.add(Path::new("nested/bob.txt"), &oid, stat.clone());

        index.add(Path::new("nested"), &oid, stat.clone());

        assert_eq!(
            vec![Path::new("alice.txt"), Path::new("nested")],
            index.entries.keys().collect::<Vec<_>>(),
        );

        Ok(())
    }

    #[test]
    fn recursive_file_replacement() -> Result<()> {
        let (mut index, oid, stat) = build_index()?;
        index.add(Path::new("alice.txt"), &oid, stat.clone());
        index.add(Path::new("nested/bob.txt"), &oid, stat.clone());
        index.add(Path::new("nested/inner/claire.txt"), &oid, stat.clone());

        index.add(Path::new("nested"), &oid, stat.clone());

        assert_eq!(
            vec![Path::new("alice.txt"), Path::new("nested")],
            index.entries.keys().collect::<Vec<_>>(),
        );

        Ok(())
    }
}
