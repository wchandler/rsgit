use std::path::Path;

pub fn parent_directories<'a>(path: &'a Path) -> Option<impl Iterator<Item = &'a Path>> {
    path.parent()
        .map(|p| p.ancestors().filter(|a| !a.as_os_str().is_empty()))
}
